import { TestBed } from '@angular/core/testing';

import { NavClickEventService } from './nav-click-event.service';

describe('NavClickEventService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NavClickEventService = TestBed.get(NavClickEventService);
    expect(service).toBeTruthy();
  });
});
