import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TreeViewComponent } from './tree-view/tree-view.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { NavigationComponent } from './navigation/navigation.component';
import { NavViewComponent } from './nav-view/nav-view.component';

@NgModule({
  declarations: [TreeViewComponent, HeaderComponent, FooterComponent, NavigationComponent, NavViewComponent],
  imports: [CommonModule],
  exports: [TreeViewComponent, HeaderComponent, FooterComponent, NavigationComponent, NavViewComponent],
})
export class ShareModuleModule { }
