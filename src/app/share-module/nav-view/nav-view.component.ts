import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav-view',
  templateUrl: './nav-view.component.html',
  styleUrls: ['./nav-view.component.scss']
})
export class NavViewComponent implements OnInit {

  public viewElement = ['Rack', 'name', 'id', 'location', 'IP'];
  constructor() { }

  ngOnInit() {
  }

}
