import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tree-view',
  templateUrl: './tree-view.component.html',
  styleUrls: ['./tree-view.component.scss']
})
export class TreeViewComponent implements OnInit {

  public treeElement = ['Rack', 'drawer', 'system', 'switch', 'NVMeof'];
  constructor() { }

  ngOnInit() {
  }

}
