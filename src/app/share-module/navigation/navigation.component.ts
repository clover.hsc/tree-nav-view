import { NavClickEventService } from '../../nav-click-event.service';
import { Component, OnInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent implements OnInit {
  constructor(private router: Router, private el: ElementRef, private switchViewMode: NavClickEventService) { }

  ngOnInit() { }

  navto(event: MouseEvent, preTgt: string) {
    this.removeTag();
    this.router.navigate([`/home/${preTgt}`]);
    this.el.nativeElement.querySelector(`#${(event.target as Element).id}`).classList.add('locate');
  }

  /**
   * A service to send click view mode button
   * @param mode : string. view mode name.
   */
  switchView(mode: string) {
    this.switchViewMode.clickViewModeEvent(mode);
  }

  /**
   * remove locate class in navigate.
   */
  removeTag() {
    const locateAry$: NodeList = this.el.nativeElement.querySelectorAll('.locate');
    locateAry$.forEach((ele: HTMLElement) => {
      ele.classList.remove('locate');
    });
  }
}
