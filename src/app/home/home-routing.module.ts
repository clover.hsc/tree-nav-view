import { PodViewComponent } from './pod-view/pod-view.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [

  {
    path: '',
    component: HomeComponent,
    children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'podview', component: PodViewComponent },
      { path: 'monitoring', loadChildren: () => import('./monitoring/monitoring.module').then((m) => m.MonitoringModule) },
      { path: '', redirectTo: 'podview', pathMatch: 'full' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule { }
