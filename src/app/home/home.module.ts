import { ShareModuleModule } from './../share-module/share-module.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PodViewComponent } from './pod-view/pod-view.component';

@NgModule({
  declarations: [HomeComponent, DashboardComponent, PodViewComponent],
  imports: [CommonModule, HomeRoutingModule, ShareModuleModule],
})
export class HomeModule { }
