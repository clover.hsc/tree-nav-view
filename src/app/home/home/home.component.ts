import { NavClickEventService } from './../../nav-click-event.service';
import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

import { Subscription } from 'rxjs';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  sub: Subscription;
  constructor(private router: Router, private el: ElementRef, private switchViewMode: NavClickEventService) {
    this.sub = this.switchViewMode.clickViewSource$.subscribe(res => console.log(res));
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  navto(event: MouseEvent, preTgt: string) {
    this.router.navigate([`/home/${preTgt}`]);
    this.el.nativeElement.querySelector(`#${(event.target as Element).id}`).classList.add('locate');
  }

  switchView(mode: string) {
    this.switchViewMode.clickViewModeEvent(mode);
  }
}
