import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavClickEventService } from './../../nav-click-event.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  sub: Subscription;
  constructor(private clickObs: NavClickEventService) {
    this.sub = this.clickObs.clickViewSource$.subscribe(res => console.log(res));
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  test(str: string) {
    this.clickObs.clickViewModeEvent(str);
  }
}
