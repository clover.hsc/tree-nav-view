import { ShareModuleModule } from './../../share-module/share-module.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MonitoringRoutingModule } from './monitoring-routing.module';
import { MonitorComponent } from './monitor/monitor.component';


@NgModule({
  declarations: [MonitorComponent],
  imports: [
    CommonModule,
    MonitoringRoutingModule,
    ShareModuleModule
  ]
})
export class MonitoringModule { }
