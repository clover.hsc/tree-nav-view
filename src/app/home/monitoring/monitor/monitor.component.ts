import { NavClickEventService } from './../../../nav-click-event.service';
import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';

import { Subscription } from 'rxjs';

@Component({
  selector: 'app-monitor',
  templateUrl: './monitor.component.html',
  styleUrls: ['./monitor.component.scss']
})
export class MonitorComponent implements OnInit, OnDestroy {

  openTreeView = true;
  openNavView = false;

  sub: Subscription;
  constructor(private clickObj: NavClickEventService, private el: ElementRef) {
    this.sub = this.clickObj.clickViewSource$.subscribe(res => {
      console.warn(res);
      // switch mode !
      if (res === 'tree') {
        if (this.openTreeView) {
          this.el.nativeElement.querySelector('#tree-view').classList.remove('none');
          this.el.nativeElement.querySelector('#monitoring').classList.add('unvisible');
          this.openTreeView = false;
          this.openNavView = false;
        } else {
          this.el.nativeElement.querySelector('#nav-view').classList.add('none');
          this.el.nativeElement.querySelector('#tree-view').classList.remove('none');
          if (!this.openNavView) {
            this.el.nativeElement.querySelector('#monitoring').classList.remove('unvisible');
          }
          this.openTreeView = true;
          this.openNavView = false;
        }
      } else {
        console.log(`open tree view: ${this.openTreeView}, opoen nav view: ${this.openNavView}`);
        if (this.openNavView) {
          this.el.nativeElement.querySelector('#nav-view').classList.remove('none');
          this.el.nativeElement.querySelector('#monitoring').classList.add('unvisible');
          this.openNavView = false;
          this.openTreeView = false;
        } else {
          this.el.nativeElement.querySelector('#tree-view').classList.add('none');
          this.el.nativeElement.querySelector('#nav-view').classList.remove('none');
          if (!this.openTreeView) {
            this.el.nativeElement.querySelector('#monitoring').classList.remove('unvisible');
          }
          this.openNavView = true;
          this.openTreeView = false;
        }
      }
    });
  }

  ngOnInit() {
  }

  /**
   * for test bidirection service.
   * @param str : string.
   */
  test(str: string) {
    this.clickObj.clickViewModeEvent(str);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
