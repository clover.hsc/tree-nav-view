import { NavClickEventService } from './../../nav-click-event.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-pod-view',
  templateUrl: './pod-view.component.html',
  styleUrls: ['./pod-view.component.scss']
})
export class PodViewComponent implements OnInit, OnDestroy {

  sub: Subscription;
  constructor(private clickObs: NavClickEventService) {
    this.sub = this.clickObs.clickViewSource$.subscribe(res => console.log(res))
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();

  }

  test(str: string) {
    this.clickObs.clickViewModeEvent(str);
  }
}
