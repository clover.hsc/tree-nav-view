import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavClickEventService {

  // Observable string source
  private clickViewSource = new Subject<string>();

  clickViewSource$ = this.clickViewSource.asObservable();

  constructor() { }

  /**
   * When user click tree view or navigate view in navigator bar.
   * Send this event to component who need to switch mode on page.
   * @param mode : 'tree' | 'navigate' string.
   */
  clickViewModeEvent(mode: string) {
    this.clickViewSource.next(mode);
  }
}
